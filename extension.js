const Gio = imports.gi.Gio;
const St = imports.gi.St;
const Main = imports.ui.main;
const Util = imports.misc.util;
const ExtensionUtils = imports.misc.extensionUtils;

const Me = ExtensionUtils.getCurrentExtension();

let button;

// Function to start/show application
function _showPlanner() {
    Util.spawnCommandLine("flatpak run com.github.alainm23.planner")
}

function init() {
  log(`Initializing ${Me.metadata.name} version ${Me.metadata.version}`);

  // Create new button
  button = new St.Bin({ style_class: 'panel-button',
                        reactive: true,
                        can_focus: true,
                        x_expand: true,
                        y_expand: false,
                        track_hover: true });

  // Create new icon object
  let icon = new St.Icon({ style_class: 'planner-icon' });

  button.set_child(icon);
  button.connect('button-press-event', _showPlanner);
}

// Enable extension
function enable() {
  log(`Enabling ${Me.metadata.name} version ${Me.metadata.version}`);
  Main.panel._rightBox.insert_child_at_index(button, 0);
}

// Disable extension
function disable() {
  log(`Disabling ${Me.metadata.name} version ${Me.metadata.version}`);
  Main.panel._rightBox.remove_child(button);
}
